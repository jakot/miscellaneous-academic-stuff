Every file contains solution to some numerical problem, these are:

- [delaunay_triang_voronoi.ipynb](delaunay_triang_voronoi.ipynb) - finding Delaunay triangulation for a set of points on plane and computing voronoi diagrams by making dual graph.
- [trig_interp.ipynb](trig_interp.ipynb) - finding interpolating function using trigonometric approach and a FFT algorithm
- [LV_odes_learning_terms.ipynb](LV_odes_learning_terms.ipynb) - numerically solving Lotka-Volterra equations with additional constraint - learning terms
